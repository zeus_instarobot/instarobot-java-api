# instarobot-java-api

InstaRobot REST API lib
A simple InstaRobot REST client library for Java

Documentation:

https://gitlab.com/zeus_instarobot/instarobot-java-api/-/wikis/API

## Usage example

```java

@Slf4j
public class ExampleMasslooking {
    public static void main(String[] args) {
        InstaRobot robot = new InstaRobot("<token>");
        User user = robot.user().me();
        logger.debug("Balance: " + user.getBalance());
        
        AccountCreationResult result = robot.accounts.addAccount("<username>", "<password>", Proxy.autoProxy());

        Account acc = robot.accounts().all().get(0);

        MassStoriesActionsData data = robot.masslooking().findByAccountId(acc.getId());
      
        data
            .getSearchDataInfo()
            .setGettingStoriesWorkModeType(MassLookingType.VIEW_ONLY_NEW);
        data
            .getSearchDataInfo()
            .setHashtagsData(Arrays.asList("moscow"));

        data = robot.masslooking().update(acc.getId(), data);
        logger.debug("Masslooking settings: " + data);
        
        robot.masslooking().start(acc.getId());
    }
}

```