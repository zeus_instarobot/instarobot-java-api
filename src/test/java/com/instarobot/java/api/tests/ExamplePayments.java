package com.instarobot.java.api.tests;

import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.ServiceSection;
import com.instarobot.java.api.model.payments.PaymentRequest;
import com.instarobot.java.api.model.payments.ServiceSlot;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class ExamplePayments {
    public static void main(String[] args) {
        InstaRobot robot = new InstaRobot(System.getenv("api.token"), System.getenv("api.host"));
        System.out.println(robot.user().me());

        System.out.println(robot.payments().buySlots(
                PaymentRequest.builder()
                        .days(30)
                        .platform(ServiceSection.MASS_STORIES_LOOKING)
                        .vip(true)
                        .count(1)
                        .build()
                )
        );

        List<ServiceSlot> slots = robot.payments().slots();

        robot.payments().prolongSlots(
                PaymentRequest.builder()
                        .days(7)
                        .platform(ServiceSection.MASS_STORIES_LOOKING)
                        .slots(slots.stream().map(ServiceSlot::getId).collect(Collectors.toList()))
                        .vip(true)
                        .build()
        );


        robot.payments().bindSlotToAccount(slots.get(0).getId(), robot.accounts().all().get(0).getId());
    }
}
