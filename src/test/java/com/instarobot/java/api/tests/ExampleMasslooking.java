package com.instarobot.java.api.tests;

import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.Account;
import com.instarobot.java.api.model.massstories.MassLookingType;
import com.instarobot.java.api.model.massstories.MassStoriesActionsData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExampleMasslooking {
    public static void main(String[] args) {
        InstaRobot robot = new InstaRobot(System.getenv("api.token"), System.getenv("api.host"));
        System.out.println(robot.user().me());
        Account acc = robot.accounts().all().get(0);

        System.out.println(acc);
        MassStoriesActionsData data = robot.masslooking().findByAccountId(acc.getId());
        System.out.println(data);
        data.getSearchDataInfo().setGettingStoriesWorkModeType(MassLookingType.VIEW_ONLY_NEW);
        data = robot.masslooking().update(acc.getId(), data);
        System.out.println(data);

        robot.masslooking().start(acc.getId());
    }
}
