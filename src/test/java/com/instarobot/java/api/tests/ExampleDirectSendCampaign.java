package com.instarobot.java.api.tests;

import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.Account;
import com.instarobot.java.api.model.direct.DirectSendCampaign;
import com.instarobot.java.api.model.massstories.MassLookingType;
import com.instarobot.java.api.model.massstories.MassStoriesActionsData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExampleDirectSendCampaign {
    public static void main(String[] args) {
        InstaRobot robot = new InstaRobot(System.getenv("api.token"), System.getenv("api.host"));
        System.out.println(robot.user().me());
        System.out.println(robot.directCampaigns().getAll());

        DirectSendCampaign campaign1 = new DirectSendCampaign();
        campaign1.setName("test created 2");

        DirectSendCampaign campaign2 = robot.directCampaigns().create(campaign1);
        System.out.println(campaign2);
        DirectSendCampaign campaign3 = robot.directCampaigns().start(campaign2.getId());
        System.out.println(campaign3);

    }
}
