package com.instarobot.java.api.tests;

import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.Account;
import com.instarobot.java.api.model.Proxy;
import com.instarobot.java.api.model.ig.AccountCreationResult;
import com.instarobot.java.api.model.ig.CheckpointInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExampleCheckpoint {
    public static void main(String[] args) {
        InstaRobot robot = new InstaRobot(System.getenv("api.token"), System.getenv("api.host"));
        AccountCreationResult data = robot.accounts().addAccount(System.getenv("example.login"), System.getenv("example.password"), Proxy.autoProxy());
        Account acc = robot.accounts().all().get(0);

        if (acc.getStatus().equals("CHALLENGE_REQUIRED")) {
            CheckpointInfo currentState = robot.checkpoints().available(acc.getId());
            System.out.println(currentState);
            if ("select_verify_method".equals(currentState.getStep_name())) {
                //select verification method
                CheckpointInfo requestCode = robot.checkpoints().requestCodeToEmail(acc.getId());
                System.out.println(requestCode);
            }
            if ("verify_email".equals(currentState.getStep_name())) {
                //enter code
                String checkCode = robot.checkpoints().checkCode(acc.getId(), "705639");
                System.out.println("CHECK CODE:" + checkCode);
            }
        }
    }
}
