package com.instarobot.java.api.exceptions;

public class ApiException extends RuntimeException {
    public ApiException() {
    }

    public ApiException(String s) {
        super(s);
    }
}
