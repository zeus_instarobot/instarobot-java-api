package com.instarobot.java.api.model.massstories;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class InstagramUserInfo {

    @JsonProperty(value = "username")
    private String userName;
    @JsonProperty(value = "userid")
    private String userId;
}
