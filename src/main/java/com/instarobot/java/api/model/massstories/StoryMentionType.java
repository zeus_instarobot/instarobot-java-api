package com.instarobot.java.api.model.massstories;

public enum StoryMentionType {
    USER, HASHTAG
}
