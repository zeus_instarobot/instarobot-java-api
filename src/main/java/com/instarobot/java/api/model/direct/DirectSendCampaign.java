package com.instarobot.java.api.model.direct;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.instarobot.java.api.model.massstories.FiltrationParameters;
import com.instarobot.java.api.model.massstories.InstagramUserInfo;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString(exclude = "sendData")
@RequiredArgsConstructor
public class DirectSendCampaign {

    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private Long id;
    @JsonProperty("started")
    private boolean started;
    @JsonProperty("filtration_parameters")
    private FiltrationParameters filtrationParameters;

    @JsonProperty("usa_ignore_list")
    private boolean useIgnoreList;

    @JsonProperty("competitors")
    private List<String> competitors;
    @JsonProperty("hashtags")
    private List<String> hashtags;
    @JsonProperty("geo_points")
    private List<String> geoPoints;

    @JsonProperty("message_text")
    private String messageText;

    @JsonProperty("messages_per_day")
    private Integer messagesCountOnDay;

    @JsonProperty("attached_post")
    private InstagramMediaInfo attachedPost;

    @JsonProperty("attached_user_profile")
    private InstagramUserInfo attachedUserProfile;

    @JsonProperty("campaign_search_data_type")
    private SearchTargetDataType directSpamDataType;

    @JsonProperty("user_list_data")
    private String sendData;

    @JsonProperty("message_type")
    private MessageType messageType = MessageType.TEXT;

    @JsonProperty("additional_rate_for_price_per_message")
    private BigDecimal additionalRateForPricePerMessage;

    @JsonProperty("day_start_time")
    private Date start;

    @JsonProperty("day_end_time")
    private Date end;

    @JsonProperty("work_round_the_clock")
    private Boolean workRoundTheClock;
}
