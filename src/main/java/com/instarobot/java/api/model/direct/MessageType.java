package com.instarobot.java.api.model.direct;

public enum MessageType {
    PROFILE,
    AUDIO,
    MEDIA,
    TEXT,
    IMAGE
}
