package com.instarobot.java.api.model.massstories;

public enum MassLookingType {

    VIEW_ALL,
    VIEW_LAST,
    VIEW_ONLY_NEW
}
