package com.instarobot.java.api.model.massstories;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MassStoriesActionsData {

    @JsonProperty("is_task_started")
    private boolean started;
    @JsonProperty("search_data_info")
    private SearchDataInfo searchDataInfo;

    @JsonProperty("masslooking_data_info")
    private MassLookingDataInfo massLookingDataInfo;

    @JsonProperty("massvoting_data_info")
    private MassVotingDataInfo massVotingDataInfo;

    @JsonProperty("massmentions_data_info")
    private MassMentionsDataInfo massMentionsDataInfo;

    @JsonProperty("ignore_list_enabled")
    private boolean useIgnoreList = false;
    @JsonProperty("ignore_list_days_of_storage")
    private int numberOfDaysOfStorage = 2;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class SearchDataInfo {
        @JsonProperty("gathering_people_work_mode_type")
        private GatheringType gatheringType;
        @JsonProperty("getting_stories_work_mode_type")
        private MassLookingType gettingStoriesWorkModeType;

        @JsonProperty("instagram_accounts_targets")
        private List<String> competitorsData;
        @JsonProperty("hashtags_targets")
        private List<String> hashtagsData;
        @JsonProperty("location_ids_targets")
        private List<String> geoPointsData;
        @JsonProperty("imported_users_list_size")
        private Integer importedUsersCount;

        @JsonProperty("filter_parameters")
        private FiltrationParameters filtrationParameters;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class MassLookingDataInfo {

        @JsonProperty("stories_viewing_enabled")
        private Boolean storiesViewingEnabled;
        @JsonProperty("stories_viewing_pause_in_minutes_in_case_of_429_error")
        private Integer pauseInMinutesInCaseOfHighSpeedProblems;
        @JsonProperty("stories_viewing_speed")
        private MassLookingSpeed massLookingSpeed;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class MassVotingDataInfo {

        @JsonProperty("question_answers_enabled")
        private Boolean storiesAutomaticQuestionAnswersEnabled = false;
        @JsonProperty("quiz_answers_enabled")
        private Boolean storiesAutomaticQuizAnswersEnabled = false;
        @JsonProperty("polls_voting_enabled")
        private Boolean storiesAutomaticPollVotingEnabled = false;
        @JsonProperty("slder_voting_enabled")
        private Boolean storiesAutomaticSliderVotingEnabled = false;
        @JsonProperty("countdown_voting_enabled")
        private Boolean storiesAutomaticSliderCountDownEnabled = false;

        @JsonProperty("question_answers_text")
        private String storyAnswers;
        @JsonProperty("slider_voting_max_allowed_value")
        private Integer sliderHighValue = 100;
        @JsonProperty("slider_voting_min_allowed_value")
        private Integer sliderLowValue = 0;

        @JsonProperty("polls_answer_option")
        private MassVotingAnswerOption pollsAnswerOption = MassVotingAnswerOption.RANDOM;
        @JsonProperty("quizzes_answer_option")
        private MassVotingAnswerOption quizzesAnswerOption = MassVotingAnswerOption.RANDOM;
        @JsonProperty("question_answers_work_mode_type")
        private MassLookingAnswerType massLookingAnswerType;

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Data
    public static class MassMentionsDataInfo {

        @JsonProperty("mass_mentions_enabled")
        private Boolean peopleMentionsInStoriesEnabled = false;
        @JsonProperty("story_background_type")
        private StoryBackground peopleMentionStoryBackground = StoryBackground.DEFAULT;
        @JsonProperty("story_type")
        private StoryType peopleMentionStoryType = StoryType.QUESTION;
        @JsonProperty("story_question_text")
        private String peopleMentionStoryQuestionText = "Hello %%%emoji%%%";
        @JsonProperty("mentioned_users_size_in_one_story")
        private Integer peopleMentionsCountInOneStory;
        @JsonProperty("mention_image_path")
        private String storyMentionsImagePath;
        @JsonProperty("mention_user_on_every_story")
        private Boolean mentionCertainUserAsFirstOnStory = false;
        @JsonProperty("mention_user_on_every_story_user")
        private InstagramUserInfo mentionUserInfo;

        @JsonProperty("mention_background_images")
        private List<String> storyMentionsImages;
        @JsonProperty("randomize_photo_before_posting")
        private Boolean randomizePhotoBeforePosting = Boolean.FALSE;
        @JsonProperty("mention_type")
        private StoryMentionType peopleMentionMainMentionType = StoryMentionType.USER;
        @JsonProperty("mention_hashtag_link")
        private String peopleMentionMainMentionHashtag;
    }
}
