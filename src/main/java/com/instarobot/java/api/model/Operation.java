package com.instarobot.java.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@ToString
public class Operation {

    private Boolean success;
    private String error;

    public Operation() {
    }

    public Operation(Boolean success) {
        this.success = success;
    }

    public Operation(Boolean success, String error) {
        this.success = success;
        this.error = error;
    }
}
