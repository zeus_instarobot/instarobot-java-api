package com.instarobot.java.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Account extends Model {

    @JsonProperty("id")
    private String id;

    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;

    @JsonProperty("photo_url")
    private String avatar;

    @JsonProperty("proxy")
    private Proxy proxy;

    @JsonProperty("status")
    private String status;;
}
