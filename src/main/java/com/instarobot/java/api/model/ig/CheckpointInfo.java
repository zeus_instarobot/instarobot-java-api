package com.instarobot.java.api.model.ig;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class CheckpointInfo extends StatusResult {
    private String step_name;
    private StepData step_data;
    private Long user_id;
    private String nonce_code;
    private String action;

    @Getter
    @Setter
    @ToString(callSuper = true)
    public static class StepData {
        private String choice;
        private String fb_access_token;
        private String email;
        private String phone_number;
        private String big_blue_token;
        private String google_oauth_token;
        private String security_code;
        private String contact_point;
        private String form_type;
    }
}
