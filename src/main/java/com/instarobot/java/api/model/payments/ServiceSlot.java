package com.instarobot.java.api.model.payments;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@ToString
public class ServiceSlot {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("end_date")
    private Date endDate;
    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("vip")
    private Boolean vip;
    @JsonProperty("account_id")
    private String accountId;
    @JsonProperty("frozen")
    private Boolean frozen;
    @JsonProperty("frozen_days")
    private Integer frozenDays;

    public ServiceSlot() {

    }
}
