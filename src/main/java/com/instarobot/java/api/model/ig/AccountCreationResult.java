package com.instarobot.java.api.model.ig;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.instarobot.java.api.model.Account;
import com.instarobot.java.api.model.Model;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountCreationResult extends Model {

    @JsonProperty("result")
    private String result;

    @JsonProperty("business_status")
    private String businessStatus;

    @JsonProperty("account_id")
    private String accountId;

    @JsonProperty("username")
    private String username;

    @JsonProperty("account_info")
    private Account account;

}
