package com.instarobot.java.api.model.massstories;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FiltrationParameters {

    public FiltrationParameters() {

    }

    private boolean checkFollowersCount;

    private Integer minFollowersCount;

    private Integer maxFollowersCount;

    private boolean checkFollowsCount;

    private Integer minFollowsCount;

    private Integer maxFollowsCount;

    private boolean checkMediaCounts;

    private Integer minMediaCounts;

    private Integer maxMediaCounts;

    private boolean onlyWithAvatar;

    private boolean withoutLinks;

    private boolean checkLanguages;

    private boolean checkGeoCities;

    private String geoCities;

    private boolean checkLastPost;

    private Integer lastPostMin;

    private Integer lastPostMax;

    private boolean ignoreBusiness;

    private boolean ignorePrivate;

    private boolean checkStopWords;

    private boolean checkWhiteWords;

    private Set<String> languagesAll;
}
