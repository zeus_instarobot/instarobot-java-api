package com.instarobot.java.api.model;

public enum ServiceSection {
    MASSFOLLOWER,
    AUTORESPONDER,
    SPAMBOT,
    MASS_STORIES_LOOKING,
}
