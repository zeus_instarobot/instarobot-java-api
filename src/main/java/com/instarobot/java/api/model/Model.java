package com.instarobot.java.api.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Model {
    private Integer statusCode;

    public Model() {
    }

    public Model(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
