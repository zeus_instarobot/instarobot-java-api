package com.instarobot.java.api.model.massstories;

public enum StoryBackground {
    DEFAULT, CUSTOM_PHOTO, RANDOM_PHOTO
}
