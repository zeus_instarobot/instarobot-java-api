package com.instarobot.java.api.model.massstories;

public enum GatheringType {
    AUTOMATIC,
    USER_LIST
}
