package com.instarobot.java.api.model.massstories;

public enum MassVotingAnswerOption {
    RANDOM,
    MOST_POPULAR,
    MOST_UNPOPULAR
}
