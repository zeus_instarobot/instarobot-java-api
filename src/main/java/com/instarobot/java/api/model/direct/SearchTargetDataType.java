package com.instarobot.java.api.model.direct;

public enum SearchTargetDataType {
    ONLINE,
    ON_LIST
}
