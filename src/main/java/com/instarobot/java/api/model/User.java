package com.instarobot.java.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @JsonProperty("email")
    private String email;

    @JsonProperty("balance")
    private BigDecimal balance;

    @JsonProperty("registration_date")
    private Date registrationDate;

    @JsonProperty("accounts_count")
    private Integer accountsCount;

}
