package com.instarobot.java.api.model.massstories;

public enum MassLookingSpeed {
    LOW, MIDDLE, HIGH, MAX
}
