package com.instarobot.java.api.model.payments;

import com.instarobot.java.api.model.ServiceSection;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@Builder
public class PaymentRequest {
    private ServiceSection platform;
    private List<Long> slots;
    private String promoCode;
    private Integer days;
    private Boolean vip;
    private Integer count;

    public Integer getDays() {
        return days != null ? days : 30;
    }
}
