package com.instarobot.java.api.model.direct;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class InstagramMediaInfo {

    private Long id;
    private String mediaId;
    private String shortCode;
    private String userId;
}
