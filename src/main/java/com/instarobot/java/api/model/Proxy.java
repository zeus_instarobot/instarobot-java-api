package com.instarobot.java.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Proxy extends Model {

    @JsonProperty("auto_proxy")
    private Boolean autoProxy;

    @JsonProperty("server_ip")
    private String serverIp;

    @JsonProperty("server_port")
    private String serverPort;

    @JsonProperty("location")
    private String proxyLocation;

    public static Proxy autoProxy() {
        Proxy proxy = new Proxy();
        proxy.setAutoProxy(Boolean.TRUE);
        return proxy;
    }
}
