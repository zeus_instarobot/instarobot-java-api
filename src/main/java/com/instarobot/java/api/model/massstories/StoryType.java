package com.instarobot.java.api.model.massstories;

public enum StoryType {
    NO_STICKER, QUESTION, BASE, POLL, SLIDER
}
