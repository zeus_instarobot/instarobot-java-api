package com.instarobot.java.api.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.ig.AccountCreationResult;
import com.instarobot.java.api.model.Account;
import com.instarobot.java.api.model.Proxy;

import java.util.HashMap;
import java.util.List;

public class AccountsApi extends AbstractApi {
    public AccountsApi(InstaRobot client) {
        super(client);
    }

    public List<Account> all() {

        return executeGet(new HashMap<>(), "/api/v1/accounts/", new TypeReference<List<Account>>() {
        });
    }

    public Account getById(String id) {

        return executeGet(new HashMap<>(), "/api/v1/accounts/" + id + "/", Account.class);
    }

    public AccountCreationResult addAccount(String login, String password, Proxy proxyDto) {
        Account account = new Account();
        account.setUsername(login);
        account.setPassword(password);
        account.setProxy(proxyDto);
        return executePost(account, new HashMap<>(), "/api/v1/accounts/", AccountCreationResult.class);
    }
}
