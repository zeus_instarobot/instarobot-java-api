package com.instarobot.java.api.api;

import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.massstories.MassStoriesActionsData;

import java.util.HashMap;

public class MentionCampaignsApi extends AbstractApi {
    public MentionCampaignsApi(InstaRobot client) {
        super(client);
    }

    public MassStoriesActionsData findByAccountId(String id) {

        return executeGet("/api/v1/accounts/" + id + "/masslooking/", MassStoriesActionsData.class);
    }

    public MassStoriesActionsData update(String id, MassStoriesActionsData update) {

        return executePost(update, new HashMap<>(), "/api/v1/accounts/" + id + "/masslooking/", MassStoriesActionsData.class);
    }

    public MassStoriesActionsData start(String id) {

        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/masslooking/start", MassStoriesActionsData.class);
    }

    public MassStoriesActionsData stop(String id) {

        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/masslooking/stop", MassStoriesActionsData.class);
    }
}
