package com.instarobot.java.api.api;

import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.ig.CheckpointInfo;

import java.util.HashMap;

public class CheckpointsApi extends AbstractApi {

    public CheckpointsApi(InstaRobot client) {
        super(client);
    }

    public CheckpointInfo requestCodeToEmail(String id) {
        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/checkpoints/request/email", CheckpointInfo.class);
    }

    public CheckpointInfo requestCodeToPhone(String id) {
        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/checkpoints/request/phone", CheckpointInfo.class);
    }

    public CheckpointInfo requestCodeTo(String id, Integer choice) {
        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/checkpoints/request/send/" + choice + "/", CheckpointInfo.class);
    }

    public String checkCode(String id, String code) {
        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/checkpoints/code/check/" + code, String.class);
    }

    public CheckpointInfo retryCodeRequest(String id, String choice) {
        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/checkpoints/code/retry", CheckpointInfo.class);
    }

    public CheckpointInfo backToVerificationMethodsSelect(String id) {
        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/checkpoints/select/back", CheckpointInfo.class);
    }

    public CheckpointInfo available(String id) {
        return executePost("", new HashMap<>(), "/api/v1/accounts/" + id + "/checkpoints/select/available", CheckpointInfo.class);
    }
}
