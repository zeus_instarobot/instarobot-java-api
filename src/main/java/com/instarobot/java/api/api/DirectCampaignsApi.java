package com.instarobot.java.api.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.direct.DirectSendCampaign;

import java.util.HashMap;
import java.util.List;

public class DirectCampaignsApi extends AbstractApi {
    public DirectCampaignsApi(InstaRobot client) {
        super(client);
    }

    public List<DirectSendCampaign> getAll() {

        return executeGet(new HashMap<>(), "/api/v1/direct/cpa/", new TypeReference<List<DirectSendCampaign>>() {
        });
    }

    public DirectSendCampaign create(DirectSendCampaign update) {

        return executePut(update, new HashMap<>(), "/api/v1/direct/cpa/", DirectSendCampaign.class);
    }


    public DirectSendCampaign update(DirectSendCampaign update) {

        return executePost(update, new HashMap<>(), "/api/v1/direct/cpa/", DirectSendCampaign.class);
    }

    public DirectSendCampaign start(Long id) {

        return executePost("", new HashMap<>(), "/api/v1/direct/cpa/" + id + "/start", DirectSendCampaign.class);
    }

    public DirectSendCampaign delete(Long id) {

        return executeDelete( new HashMap<>(), "/api/v1/direct/cpa/" + id + "/", DirectSendCampaign.class);
    }

    public DirectSendCampaign stop(Long id) {

        return executePost("", new HashMap<>(), "/api/v1/direct/cpa/" + id + "/stop", DirectSendCampaign.class);
    }
}
