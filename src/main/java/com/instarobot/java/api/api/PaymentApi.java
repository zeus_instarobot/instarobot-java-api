package com.instarobot.java.api.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.Operation;
import com.instarobot.java.api.model.payments.PaymentRequest;
import com.instarobot.java.api.model.payments.ServiceSlot;

import java.util.HashMap;
import java.util.List;

public class PaymentApi extends AbstractApi {

    public PaymentApi(InstaRobot client) {
        super(client);
    }

    public List<ServiceSlot> slots() {
        return executeGet(new HashMap<>(), "/api/v1/payments/slots/", new TypeReference<List<ServiceSlot>>() {
        });
    }

    public Operation buySlots(PaymentRequest request) {
        return executePost(request, new HashMap<>(), "/api/v1/payments/slots/buy/", Operation.class);
    }

    public Operation prolongSlots(PaymentRequest request) {
        return executePost(request, new HashMap<>(), "/api/v1/payments/slots/prolongation/", Operation.class);
    }

    public Operation bindSlotToAccount(Long slot, String accountId) {
        return executePost("", new HashMap<>(), "/api/v1/payments/slots/" + slot + "/bind/" + accountId + "/", Operation.class);
    }

    public Operation unbindAccountFromSlot(Long slot) {
        return executePost("", new HashMap<>(), "/api/v1/payments/slots/" + slot + "/unbind/", Operation.class);
    }
}
