package com.instarobot.java.api.api;

import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.model.Account;
import com.instarobot.java.api.model.User;

import java.util.HashMap;

public class UserApi extends AbstractApi {

    public UserApi(InstaRobot client) {
        super(client);
    }

    public User me() {
        return executeGet(new HashMap<>(), "/api/v1/user/", User.class);
    }
}
