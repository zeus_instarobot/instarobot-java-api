package com.instarobot.java.api.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.instarobot.java.api.InstaRobot;
import com.instarobot.java.api.exceptions.ApiException;
import com.instarobot.java.api.model.Model;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class AbstractApi {
    protected InstaRobot instaRobot;
    protected ObjectMapper objectMapper;

    public AbstractApi(InstaRobot client) {
        this.instaRobot = client;
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    }

    @SneakyThrows
    protected <T> T executeGet(String url, Class<T> resultClass) {
        return executeGet(new HashMap<>(), url, resultClass);
    }

    @SneakyThrows
    protected <T> T executeGet(Map<String, String> params, String url, Class<T> resultClass) {
        HttpGet get = new HttpGet(instaRobot.getHost() + url);
        return handle(resultClass, get);
    }

    @SneakyThrows
    protected <T> T executeDelete(Map<String, String> params, String url, Class<T> resultClass) {
        HttpDelete get = new HttpDelete(instaRobot.getHost() + url);
        return handle(resultClass, get);
    }

    @SneakyThrows
    protected <T> T executePost(Object data, Map<String, String> params, String url, Class<T> resultClass) {
        HttpPost post = new HttpPost(instaRobot.getHost() + url);
        post.setEntity(new StringEntity(objectMapper.writeValueAsString(data)));
        post.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        return handle(resultClass, post);
    }

    @SneakyThrows
    protected <T> T executePut(Object data, Map<String, String> params, String url, Class<T> resultClass) {
        HttpPut post = new HttpPut(instaRobot.getHost() + url);
        post.setEntity(new StringEntity(objectMapper.writeValueAsString(data)));
        post.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        return handle(resultClass, post);
    }

    private <T> T handle(Class<T> resultClass, HttpUriRequest post) throws IOException {
        post.setHeader("Authorization", "Bearer " + instaRobot.getToken());
        try (CloseableHttpResponse response = instaRobot.getClient().execute(post)) {
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new ApiException("Error accessing api. Status code: " + response.getStatusLine().getStatusCode());
            }
            T result = objectMapper.readValue(response.getEntity().getContent(), resultClass);
            if (response instanceof Model) {
                ((Model) result).setStatusCode(response.getStatusLine().getStatusCode());
            }
            return result;
        }
    }

    @SneakyThrows
    protected <T> T executeGet(Map<String, String> params, String url, TypeReference<T> resultClass) {

        HttpGet get = new HttpGet(instaRobot.getHost() + url);
        get.setHeader("Authorization", "Bearer " + instaRobot.getToken());

        try (CloseableHttpResponse response = instaRobot.getClient().execute(get)) {
            return objectMapper.readValue(response.getEntity().getContent(), resultClass);
        }

    }
}
