package com.instarobot.java.api;

import com.instarobot.java.api.api.*;
import lombok.Data;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

@Data
public class InstaRobot {
    public static final String DEFAULT_API_URL = "https://gate.instarobot.pro/mainapp/public";
    private String token;
    private String host;
    private CloseableHttpClient client;

    public InstaRobot(String token) {
        this(token, DEFAULT_API_URL);
    }

    public InstaRobot(String token, String host) {
        this(token, host, HttpClients.createDefault());
    }

    public InstaRobot(String token, String host, CloseableHttpClient client) {
        this.token = token;
        this.host = host != null ? host : DEFAULT_API_URL;
        this.client = client;
    }

    public AccountsApi accounts() {
        return new AccountsApi(this);
    }

    public CheckpointsApi checkpoints() {
        return new CheckpointsApi(this);
    }

    public MassLookingApi masslooking() {
        return new MassLookingApi(this);
    }

    public DirectCampaignsApi directCampaigns() {
        return new DirectCampaignsApi(this);
    }

    public MentionCampaignsApi mentionCampaigns() {
        return new MentionCampaignsApi(this);
    }

    public UserApi user() {
        return new UserApi(this);
    }

    public PaymentApi payments() {
        return new PaymentApi(this);
    }
}
